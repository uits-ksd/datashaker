# DataShaker
A program to run a simple SQL query over and over for a set period of time.

## How To Use
First, copy
```
src/main/resources/spring-context.xml-TEMPLATE
```

to:
```
src/main/resources/spring-context.xml
```

This second file is already listed in the gitignore, so won't ever be accidentally checked in to VCS.

Next, replace all occurences of CHANGEME to valid values. Execute the following maven command while in the root of the project: 
```
mvn package
```

This will create an executable jar at within the project:
```
$PROJECT_DIR/target/datashaker.jar
```

Copy that jar to the host that has visibilty to the `datasource`, and execute the jar with the command:
```
java -jar datashaker.jar
```

The jar will run for five minutes, and output logs to `datashaker.log`, which will be output in the same directory as the jar.