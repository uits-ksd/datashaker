package edu.arizona.tools.ds;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlRowSetResultSetExtractor;
import org.springframework.jdbc.support.rowset.ResultSetWrappingSqlRowSet;

public class DataShaker {
    private static final Logger LOG = LogManager.getLogger(DataShaker.class);
    private static final String GET_ALL_IDS_QUERY = "select ROLE_ID from KRIM_ROLE_T";
    private static final String GET_SINGLE_ROW_QUERY_FORMAT = "select * from KRIM_ROLE_T where ROLE_ID = '%s'";
    private static final long RUN_DURATION = 5*60*1000; // five minutes in millis

    private JdbcTemplate jdbcTemplate;
    private long whenToQuit;
    private long totalNumQueriesRun;


    public DataShaker() {
        ApplicationContext ac = new ClassPathXmlApplicationContext("spring-context.xml");
        DataSource dataSource = (DataSource)ac.getBean("dataSource");
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.whenToQuit = Long.MIN_VALUE;
        totalNumQueriesRun = 0;
    }


    public void shakeShakeShake() {
        this.whenToQuit = System.currentTimeMillis() + RUN_DURATION;

        LOG.info("About to query for %d roles...");
        List<String> roleIds = getAllRoleIds();
        LOG.info("Found %d roleIds.", roleIds.size());

        while (!timeToQuit()) {

            LOG.info("About to loop over %d roleIds...", roleIds.size());
            for (String roleId : roleIds) {
                String sql = String.format(GET_SINGLE_ROW_QUERY_FORMAT, roleId);
                jdbcTemplate.query(sql, new SqlRowSetResultSetExtractor());
                totalNumQueriesRun++;

                if (timeToQuit()) {
                    break;
                }
            }

            if (timeToQuit()) {
                break;
            }

            LOG.info("Completed looping over roleIds; %d queries executed so far.", totalNumQueriesRun);
        }

        double runTimeInSeconds = RUN_DURATION/1000;
        double queriesPerSecond = totalNumQueriesRun/runTimeInSeconds;
        LOG.info("Completed: %d total queries executed in %.2f seconds; this is %.2f queries/s.",
                totalNumQueriesRun, runTimeInSeconds, queriesPerSecond);

    }


    private boolean timeToQuit() {
        return System.currentTimeMillis() >= whenToQuit;
    }



    private List<String> getAllRoleIds() {
        LOG.info("About to query for all role IDs...");
        List<String> idList = new ArrayList<>();

        ResultSetWrappingSqlRowSet rowSet = (ResultSetWrappingSqlRowSet)getJdbcTemplate().query(GET_ALL_IDS_QUERY, new SqlRowSetResultSetExtractor());
        while(rowSet.next()){
            idList.add((String)rowSet.getObject(1)); // ROLE_ID is the first column, 1-based
        }

        LOG.info("Complete.");

        return idList;
    }


    private JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }


    public static void main(String... sgs) {
        DataShaker dataShaker = new DataShaker();
        dataShaker.shakeShakeShake();
    }
}
